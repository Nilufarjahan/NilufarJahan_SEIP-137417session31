<?php

abstract class AbstractClass
{
    // Force Extending class to define this method
    abstract protected function getValue();
    abstract protected function prefixValue($prefix);

    // Common method
    public function printOut() {
        print $this->getValue() . "\n";
    }
}


class ABC extends AbstractClass{
    protected function getValue()
    {
        return "50";
    }
    protected function prefixValue($prefix)
    {
       echo"$prefix".$this->getValue();
    }

    public function test(){
    echo"This is a test";
        $this->prefixValue("Age");
}
}
$obj=new ABC();
$obj->test();
