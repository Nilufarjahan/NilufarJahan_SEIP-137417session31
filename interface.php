<?php
interface CanFly {
public function fly();
}

interface CanSwim {
public function swim();
}

class Bird {
public function info() {

echo "I am a {$this->name}\n";
echo "I am an bird\n";
}
}

class Penguin extends Bird implements CanSwim {
    public $name = "Penguin";
    public function swim() {
        echo "I swim\n";
    }
}
class Dove extends Bird implements CanFly {
    public $name = "Dove";
    public function fly() {
        echo "I fly\n";
    }
}

class Duck extends Bird implements CanFly, CanSwim {
    public $name = "Duck";
    public function fly() {
        echo "I fly\n";
    }
    public function swim() {
        echo "I swim\n";
    }
}
function describe($objBird)
{
    if ($objBird instanceof Bird) {
        $objBird->info();
    }


    if ($objBird instanceof CanFly) {
        $objBird->fly();
    }
    if ($objBird instanceof CanSwim) {
        $objBird->fly();
    }
}

describe(new Duck());